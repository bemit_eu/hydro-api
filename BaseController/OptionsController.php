<?php

namespace HydroApi\BaseController;

class OptionsController extends \Hydro\Controller\ApiController {

    // tmp static configs
    // todo
    public static $origin_allowed = [];

    public function __construct() {
        parent::__construct();
        // get accessing origin from header
        $current_origin = filter_input(INPUT_SERVER, 'HTTP_ORIGIN', FILTER_SANITIZE_URL);
        // set allowed origins
        $origin_allowed = static::$origin_allowed;
        if(!empty($current_origin) && in_array($current_origin, $origin_allowed)) {
            // when current is allowed, set header
            $this->addHeader('Access-Control-Allow-Origin: ' . $current_origin);
        }

        $this->addHeader('Vary: Origin');

        $this->addHeader('Access-Control-Allow-Credentials: true');
        $this->addHeader('Access-Control-Allow-Methods: OPTIONS, POST, GET, DELETE, PUT');

        $this->addHeader('Access-Control-Max-Age: 2');

        $header_allowed = [
            'Content-Type',
            'Accept',
            'AUTHORIZATION',
            'X-Requested-With',
            'X_AUTH_TOKEN',
            'X_AUTH_SIGNATURE',
            'X_API_OPTION',
            'CONTEXT_HOOK',
            'remember-me',
        ];

        $this->addHeader('Access-Control-Allow-Headers: ' . implode(', ', $header_allowed));

        $header_expose = [
            'Content-Range',
        ];

        $this->addHeader('Access-Control-Expose-Headers: ' . implode(', ', $header_expose));
    }
}