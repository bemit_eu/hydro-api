<?php

namespace HydroApi\BaseController;

use HydroFeature\Container as FeatureContainer;
use Flood\Component\Route\Container as RouteContainer;

class BaseController extends OptionsController {

    public $hook_context = false;

    public function __construct() {
        parent::__construct();

        $this->hook_context = FeatureContainer::_userActive()->getHookContext();
    }

    /**
     * Checks for valid JWT and sends corresponding header codes for login timed out (440), login not existing (401), and forbidden (403) access,
     *
     * @param $restricts
     *
     * @return bool
     */
    public function accessAllowed($restricts = false) {
        if(FeatureContainer::_userToken()::extractFromHeader()) {
            // 403
            if($restricts && call_user_func($restricts)) {
                // restricted and allowed
                return true;
            } else if($restricts) {
                // restricted but execution returned false
                $this->addHeader(RouteContainer::_url()::getStatusHeader(403));
            } else {
                // not restricted, just valid JWT needed
                return true;
            }
        } else if(FeatureContainer::_userToken()::isBearerInHeader()) {
            // login timeout
            $this->addHeader(RouteContainer::_url()::getStatusHeader(440));
        } else {
            // no valid token found
            $this->addHeader(RouteContainer::_url()::getStatusHeader(401));
        }
        return false;
    }

    public function needHookContext() {
        if($this->hook_context) {
            return true;
        }
        $this->addHeader(RouteContainer::_url()::getStatusHeader(400));
        $this->resp_data = ['error' => 'hook_context needed but not supplied'];
        $this->respondJson();
        return false;
    }
}