<?php

namespace HydroApi\Lib;

use Hydro\Container;

class HookInfo {

    /**
     * @return array id's of hooks that are allowed for the api
     */
    static public function getApiEnabled() {
        $hooks = [];
        foreach(Container::_hookStorage()->getHookList() as $hook_id) {
            /**
             * @var \Hydro\Hook\Hook $hook
             */
            $hook = Container::_hookStorage()->get($hook_id);
            if($hook->getApi() && isset($hook->getApi()['active']) && $hook->getApi()['active']) {
                $hook_info = [
                    'id' => $hook_id,
                ];

                if(isset($hook->getApi()['label']) && $hook->getApi()['label']) {
                    $hook_info['label'] = $hook->getApi()['label'];
                } else {
                    $hook_info['label'] = $hook_id;
                }

                $hooks[] = $hook_info;
            }
        }

        return $hooks;
    }
}