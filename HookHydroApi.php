<?php

namespace HydroApi;

use Flood\Captn;
use Flood\Component\Route as FloodRoute;
use Flood\Component\Route\Hook\HookFile;
use Flood\Component\Route\Hook\HookFileI;
use Hydro\Container;
use HydroApi\Action\AccessAuthSource;
use HydroApi\Action\Definition;
use HydroApi\Controller\Shop\Order;
use HydroFeature\User\AccessManager;
use HydroFeature\User\StorageAction as UserStorageAction;
use HydroFeature\Container as FeatureContainer;

class HookHydroApi extends HookFile implements HookFileI {

    public function __construct() {
        Captn\EventDispatcher::on(
            'hydro.exec.addRoutes',
            [$this, 'addRoutes']
        );

        Captn\EventDispatcher::on(
            'hydro.exec.bindComponent',
            [$this, 'setupAccessControl']
        );
    }

    public function addRoutes($route) {
        /** @var FloodRoute\Route $route */

        $routeGen = FloodRoute\Container::_hookRouteGenerator();

        $apiRoute = static function($id, $path, $defaults = [], $requirements = []) use ($route, $routeGen) {
            return (new FloodRoute\Generator\ApiRoute($id, $path, $defaults, $requirements))
                ->setCollection(static function($id, $route_item) use ($route) {
                    $route->addRoute($id, $route_item);
                })
                ->setOptionsController(static function() use ($route) {
                    $controller = new BaseController\OptionsController();
                    $controller->setResponse('');
                    $controller->respond();
                });
        };

        $this->addRoutesBackendConnect($apiRoute, $routeGen);
        $this->addRoutesAuth($apiRoute, $routeGen);
        $this->addRoutesUser($apiRoute, $routeGen);
        $this->addRoutesMessage($apiRoute, $routeGen);
        $this->addRoutesI18n($apiRoute, $routeGen);
        $this->addRoutesContent($apiRoute, $routeGen);
        $this->addRoutesShop($apiRoute, $routeGen);
        $this->addRoutesMarketing($apiRoute, $routeGen);
        $this->addRoutesFileManager($apiRoute, $routeGen);
        $this->addRoutesWorkflow($apiRoute, $routeGen);
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesBackendConnect($apiRoute, $routeGen) {
        //
        // Backend Connect

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'backend-info'),
            '/api/backend'
        )
            ->get(static function() {
                // no acl at all, to be able to setup consumer apps with any information needed for server connection
                $controller = new Controller\BackendConnect\Info();
                $controller->handleList();
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesAuth($apiRoute, $routeGen) {
        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'auth--verify'),
            '/api/auth/verify'
        )
            ->post(static function() {
                $controller = new Controller\User\Auth();
                $controller->handleVerify();
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'auth--test'),
            '/api/auth/check-allowed'
        )
            ->post(static function() {
                $controller = new Controller\User\Auth();
                $controller->handleCheckAllowed();
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesUser($apiRoute, $routeGen) {
        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'user--create'),
            '/api/user/create'
        )
            ->post(static function() {
                // acl for user is in `feature.user` and also implemented in StorageItem\User and User\User
                $controller = new Controller\User\User();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_USER) &&
                        FeatureContainer::_accessManager()->isAllowed('feature.user', AccessManager::ACTION_CREATE)
                    );
                });
                if($allowed) {
                    $controller->handleCreate();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'user--list'),
            '/api/user/list'
        )
            ->post(static function() {
                // List
                $controller = new Controller\User\User();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_USER) &&
                        FeatureContainer::_accessManager()->isAllowed('feature.user', AccessManager::ACTION_READ)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'user-roles'),
            '/api/user/roles'
        )
            ->get(static function() {
                // Get All Roles
                $controller = new Controller\User\User();
                $controller->handleListRoles();
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'user--one'),
            '/api/user/{id}'
        )
            ->get(static function() {
                // Get One User
                // acl for user is in `feature.user`
                $controller = new Controller\User\User();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_USER) &&
                        FeatureContainer::_accessManager()->isAllowed('feature.user', AccessManager::ACTION_READ, Container::_route()->match['id'])
                    );
                });
                if($allowed) {
                    $controller->handleGet(Container::_route()->match['id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Update One User
                // acl for user is in `feature.user`
                $controller = new Controller\User\User();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_USER) &&
                        FeatureContainer::_accessManager()->isAllowed('feature.user', AccessManager::ACTION_UPDATE, Container::_route()->match['id'])
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete User
                // acl for user is in `feature.user`
                $controller = new Controller\User\User();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_USER) &&
                        FeatureContainer::_accessManager()->isAllowed('feature.user', AccessManager::ACTION_DELETE, Container::_route()->match['id'])
                    );
                });
                if($allowed) {
                    $controller->handleDelete(Container::_route()->match['id']);
                } else {
                    $controller->respondEmpty();
                }
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesMessage($apiRoute, $routeGen) {
        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'message--create'),
            '/api/message/create'
        )
            ->post(static function() {
                /*$controller = new Controller\Message\Message();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MESSAGE)
                    );
                });
                if($allowed) {
                    $controller->handleCreate();
                } else {
                    $controller->respondEmpty();
                }*/
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'message--list'),
            '/api/message/list'
        )
            ->post(static function() {
                // List
                $controller = new Controller\Message\Message();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MESSAGE)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'message-mailings'),
            '/api/message/mailings'
        )
            ->get(static function() {
                // Get All Mailings
                $controller = new Controller\Message\Message();
                $controller->handleListMailings();
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'message--one'),
            '/api/message/{id}'
        )
            ->get(static function() {
                // Get One Message
                // acl for message is in `feature.message`
                $controller = new Controller\Message\Message();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MESSAGE)
                    );
                });
                if($allowed) {
                    $controller->handleGet(Container::_route()->match['id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Update One Message
                // acl for message is in `feature.message`
                /*$controller = new Controller\Message\Message();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MESSAGE)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['id']);
                } else {
                    $controller->respondEmpty();
                }*/
            })
            ->delete(static function() {
                // Delete Message
                // acl for message is in `feature.message`
                /*$controller = new Controller\Message\Message();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MESSAGE)
                    );
                });
                if($allowed) {
                    $controller->handleDelete(Container::_route()->match['id']);
                } else {
                    $controller->respondEmpty();
                }*/
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesI18n($apiRoute, $routeGen) {
        //
        // Locales

        // todo implement finish/i18n component, not just which locales exist

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'i18n-locale'),
            '/api/i18n/locale'
        )
            ->get(static function() {
                // List All Locales existing
                $controller = new Controller\I18n\Locales();
                $controller->handleList();
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesContent($apiRoute, $routeGen) {
        //
        // Section

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-section'),
            '/api/content/section'
        )
            ->get(static function() {
                // List All Sections
                $controller = new Controller\Content\Section();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            })
            ->post(static function() {
                // Create Section
                $controller = new Controller\Content\Section();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION)
                    );
                });
                if($allowed) {
                    $controller->handleCreate();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-section--single'),
            '/api/content/section/{section_id}'
        )
            ->get(static function() {
                // Get One Section
                $controller = new Controller\Content\Section();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION)
                    );
                });
                if($allowed) {
                    $controller->handleGet(Container::_route()->match['section_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Update Section
                $controller = new Controller\Content\Section();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['section_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Section
                $controller = new Controller\Content\Section();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleDelete(Container::_route()->match['section_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-section--article'),
            '/api/content/section/{section_id}/article'
        )
            ->get(static function() {
                // List All Articles of Section
                $controller = new Controller\Content\Section();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleArticleList(Container::_route()->match['section_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Article

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-article'),
            '/api/content/article'
        )
            ->get(static function() {
                // List All Articles
                $controller = new Controller\Content\Article();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }

            })
            ->post(static function() {
                // Create Article
                $controller = new Controller\Content\Article();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleCreate();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-article--one'),
            '/api/content/article/{section_id}/{article_id}'
        )
            ->get(static function() {
                // Get One Article
                $controller = new Controller\Content\Article();

                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleGetOne(Container::_route()->match['article_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Update Article BASE DATA
                $controller = new Controller\Content\Article();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['article_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Article
                $controller = new Controller\Content\Article();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleDelete(Container::_route()->match['article_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-article-add-locale'),
            '/api/content/article/{article_id}/locale/{locale}'
        )
            ->put(static function() {
                // Create Article Localised Data
                $controller = new Controller\Content\Article();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleAddLocale(Container::_route()->match['article_id'], Container::_route()->match['locale']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-article-data'),
            '/api/content/article-data/{data_id}'
        )
            ->put(static function() {
                // Update Article Localised Data
                $controller = new Controller\Content\Article();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleUpdateLocale(Container::_route()->match['data_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Article Localised Data
                $controller = new Controller\Content\Article();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleDeleteLocale(Container::_route()->match['data_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Schema

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-schema'),
            '/api/content/schema/{schema}'
        )
            ->get(static function() {
                // Get One Schema
                $controller = new Controller\Content\Schema();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT)
                    );
                });
                if($allowed) {
                    $controller->handleGetOne(Container::_route()->match['schema']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Page-Type

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-page-type'),
            '/api/content/page-type/{id}'
        )
            ->get(static function() {
                $controller = new Controller\Content\PageType();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT)
                    );
                });
                if($allowed) {
                    $controller->handleGetOne(Container::_route()->match['id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-page-type-list'),
            '/api/content/page-types'
        )
            ->get(static function() {
                $controller = new Controller\Content\PageType();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Block / Block-Selection

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-block'),
            '/api/content/block/{block}'
        )
            ->get(static function() {
                $controller = new Controller\Content\Block();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT)
                    );
                });
                if($allowed) {
                    $controller->handleGetOne(Container::_route()->match['block']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'content-block-selection'),
            '/api/content/blocks'
        )
            ->get(static function() {
                $controller = new Controller\Content\Block();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            });

    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesShop($apiRoute, $routeGen) {
        //
        // Group

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-group'),
            '/api/shop/group'
        )
            ->get(static function() {
                // List All Groups
                $controller = new Controller\Shop\Group();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            })
            ->post(static function() {
                // Create Group
                $controller = new Controller\Shop\Group();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP)
                    );
                });
                if($allowed) {
                    $controller->handleCreate();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-group--single'),
            '/api/shop/group/{group_id}'
        )
            ->get(static function() {
                // Get One Group
                $controller = new Controller\Shop\Group();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP)
                    );
                });
                if($allowed) {
                    $controller->handleGet(Container::_route()->match['group_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Update Group
                $controller = new Controller\Shop\Group();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['group_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Group
                $controller = new Controller\Shop\Group();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE)
                    );
                });
                if($allowed) {
                    $controller->handleDelete(Container::_route()->match['group_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-group--product'),
            '/api/shop/group/{group_id}/product'
        )
            ->get(static function() {
                // List All Products of Group
                $controller = new Controller\Shop\Group();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleProductList(Container::_route()->match['group_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Product

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-product'),
            '/api/shop/product'
        )
            ->get(static function() {
                // List All Products
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            })
            ->post(static function() {
                // Create Product
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleCreate();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-product--one'),
            '/api/shop/product/{product_id}'
        )
            ->get(static function() {
                // Get One Product
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleGetOne(Container::_route()->match['product_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Update Product BASE DATA
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['product_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Product
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleDelete(Container::_route()->match['product_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-product--by-product_no'),
            '/api/shop/product-by-no/{locale}'
        )
            ->post(static function() {
                // Get Products by ProductNo
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleGetByProductNo(Container::_route()->match['locale']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-product-add-locale'),
            '/api/shop/product/{product_id}/locale/{locale}'
        )
            ->put(static function() {
                // Create Product Localised Data
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleAddLocale(Container::_route()->match['product_id'], Container::_route()->match['locale']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-product-data'),
            '/api/shop/product-data/{data_id}'
        )
            ->put(static function() {
                // Update Product Localised Data
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleUpdateData(Container::_route()->match['data_id']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Product Localised Data
                $controller = new Controller\Shop\Product();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleDeleteLocale(Container::_route()->match['data_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Product Types

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-product-type'),
            '/api/shop/product-type/{type}'
        )
            ->get(static function() {
                $controller = new Controller\Shop\ProductType();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleGetOne(Container::_route()->match['type']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-product-type-list'),
            '/api/shop/product-types'
        )
            ->get(static function() {
                $controller = new Controller\Shop\ProductType();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Orders

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-order-list'),
            '/api/shop/order/list/{state}',
            ['state' => '']
        )
            ->post(static function() {
                // List Orders
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_ORDER)
                    );
                });
                if($allowed) {
                    $controller->handleList(Container::_route()->match['state']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-order-update'),
            '/api/shop/order/update/{order_id}'
        )
            ->post(static function() {
                // List Orders
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_ORDER)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['order_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'shop-order-get'),
            '/api/shop/order/get/{order_id}'
        )
            ->get(static function() {
                // List Orders
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_ORDER)
                    );
                });
                if($allowed) {
                    $controller->handleGet(Container::_route()->match['order_id']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Inventory
        //

        //
        // Inventory Warehouse

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-warehouse-list'),
            '/api/inventory/warehouses'
        )
            ->get(static function() {
                // List Inventory Warehouses
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleListWarehouses();
                } else {
                    $controller->respondEmpty();
                }
            });

        /*$apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-warehouse'),
            '/api/inventory/warehouse/{warehouse}'
        )
            ->get(static function() {
                // Get Warehouse
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['warehouse']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->post(static function() {
                // Update Warehouse
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['warehouse']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Create Warehouse
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['warehouse']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Warehouse
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['warehouse']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-warehouse-stocks-list'),
            '/api/inventory/warehouse/{warehouse}/stocks'
        )
            ->get(static function() {
                // List Stocks for Warehouse
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleList(Container::_route()->match['warehouse']);
                } else {
                    $controller->respondEmpty();
                }
            });*/

        //
        // Inventory Warehouse Stocks

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stocks-list'),
            '/api/inventory/stocks'
        )
            ->get(static function() {
                // List Stocks
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleListStocks();
                } else {
                    $controller->respondEmpty();
                }
            });

        /*$apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stocks'),
            '/api/inventory/stock/{stock}'
        )
            ->get(static function() {
                // Get Stock
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['stock']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->post(static function() {
                // Update Stock
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['stock']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->put(static function() {
                // Create Stock
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['stock']);
                } else {
                    $controller->respondEmpty();
                }
            })
            ->delete(static function() {
                // Delete Stock
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['stock']);
                } else {
                    $controller->respondEmpty();
                }
            });*/

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stock-list-items'),
            '/api/inventory/stock/{stock}/items'
        )
            ->get(static function() {
                // List Items for Stock
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleListStockItemsOfStock(Container::_route()->match['stock']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Inventory Warehouse Stock Items

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stock-items-list'),
            '/api/inventory/stock-items'
        )
            ->get(static function() {
                // List All Stock-Items
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleListStockItems();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stock-item-create'),
            '/api/inventory/stock-item'
        )
            ->post(static function() {
                // Create Stock Item
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleCreateItem();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stock-item-of-product'),
            '/api/inventory/stock-item/of-product/{product}'
        )
            ->get(static function() {
                // Get Stock Item of Shop Product
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleGetItemByProduct(Container::_route()->match['product']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stock-items'),
            '/api/inventory/stock-item/{stock_item}'
        )
            /*->get(static function() {
                // Get Stock Item
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ)
                    );
                });
                if($allowed) {
                    $controller->handleGetItem(Container::_route()->match['stock_item']);
                } else {
                    $controller->respondEmpty();
                }
            })*/
            /*->post(static function() {
                // Update Stock Item
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdateItem(Container::_route()->match['stock_item']);
                } else {
                    $controller->respondEmpty();
                }
            })*/
            /*->delete(static function() {
                // Delete Stock Item
                $controller = new Controller\Shop\Order();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdate(Container::_route()->match['stock_item']);
                } else {
                    $controller->respondEmpty();
                }
            })*/
        ;

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'inventory-stock-items'),
            '/api/inventory/stock-item-quantity/{stock_item}'
        )
            ->post(static function() {
                // Update Stock Item Quantity
                $controller = new Controller\Shop\Inventory();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN)
                    );
                });
                if($allowed) {
                    $controller->handleUpdateItemQuantity(Container::_route()->match['stock_item']);
                } else {
                    $controller->respondEmpty();
                }
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesMarketing($apiRoute, $routeGen) {
        //
        // Newsletter User

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'marketing-newsletter--user-list'),
            '/api/marketing/newsletter/user/list'
        )
            ->post(static function() {
                // List
                $controller = new Controller\Marketing\Newsletter\User();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MARKETING) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MARKETING_NEWSLETTER)
                    );
                });
                if($allowed) {
                    $controller->handleList();
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'marketing-newsletter--user'),
            '/api/marketing/newsletter/user'
        )
            ->post(static function() {
                // Create/Add Newsletter Users
                $controller = new Controller\Marketing\Newsletter\User();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MARKETING) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MARKETING_NEWSLETTER)
                    );
                });
                if($allowed) {
                    $controller->handleCreate();
                } else {
                    $controller->respondEmpty();
                }
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesFileManager($apiRoute, $routeGen) {
        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'file-manager--add-file'),
            '/api/files/add-one/{dir}',
            ['dir' => false], ['dir' => '.+']
        )
            ->post(static function() {
                $controller = new Controller\FileManager\File();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MEDIA)
                    );
                });
                if($allowed) {
                    $controller->receiveFile(Container::_route()->match['dir']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'file-manager--rename-file'),
            '/api/files/rename-file/{file}',
            ['file' => false], ['file' => '.+']
        )
            ->post(static function() {
                $controller = new Controller\FileManager\File();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MEDIA)
                    );
                });
                if($allowed) {
                    $controller->renameFile(Container::_route()->match['file']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'file-manager--delete-file'),
            '/api/files/delete-file/{file}',
            ['file' => false], ['file' => '.+']
        )
            ->post(static function() {
                $controller = new Controller\FileManager\File();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MEDIA)
                    );
                });
                if($allowed) {
                    $controller->deleteFile(Container::_route()->match['file']);
                } else {
                    $controller->respondEmpty();
                }
            });

        //
        // Dir

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'file-manager--list'),
            '/api/files/list/{dir}',
            ['dir' => false], ['dir' => '.+']
        )
            ->get(static function() {
                $controller = new Controller\FileManager\Dir();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MEDIA)
                    );
                });
                if($allowed) {
                    $controller->list(Container::_route()->match['dir']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'file-manager--create-dir'),
            '/api/files/create-dir/{dir}',
            ['dir' => false], ['dir' => '.+']
        )
            ->post(static function() {
                $controller = new Controller\FileManager\Dir();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MEDIA)
                    );
                });
                if($allowed) {
                    $controller->createDir(Container::_route()->match['dir']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'file-manager--rename-dir'),
            '/api/files/rename-dir/{dir}',
            ['dir' => false], ['dir' => '.+']
        )
            ->post(static function() {
                $controller = new Controller\FileManager\Dir();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MEDIA)
                    );
                });
                if($allowed) {
                    $controller->renameDir(Container::_route()->match['dir']);
                } else {
                    $controller->respondEmpty();
                }
            });

        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'file-manager--delete-dir'),
            '/api/files/delete-dir/{dir}',
            ['dir' => false], ['dir' => '.+']
        )
            ->post(static function() {
                $controller = new Controller\FileManager\Dir();
                $allowed = $controller->accessAllowed(static function() {
                    return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_MEDIA)
                    );
                });
                if($allowed) {
                    $controller->deleteDir(Container::_route()->match['dir']);
                } else {
                    $controller->respondEmpty();
                }
            });
    }

    /**
     * @param                                            $apiRoute
     * @param \Flood\Component\Route\Hook\RouteGenerator $routeGen
     */
    public function addRoutesWorkflow($apiRoute, $routeGen) {
        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'workflow--info'),
            '/api/workflow/info/{id}/{hook}/{from}'
        )
            ->get(static function() {
                $controller = new Controller\Workflow\Workflow();
                $allowed = $controller->accessAllowed(static function() {
                    return FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_ADMIN);
                });
                if($allowed) {
                    $controller->handleGetInfo(Container::_route()->match['id'], Container::_route()->match['hook'], Container::_route()->match['from']);
                } else {
                    $controller->respondEmpty();
                }
            });
        $apiRoute(
            $routeGen::createRouteId($this->getId(), false, 'workflow--info-transition'),
            '/api/workflow/info/{id}/{hook}/{from}/{to}'
        )
            ->get(static function() {
                $controller = new Controller\Workflow\Workflow();
                $allowed = $controller->accessAllowed(static function() {
                    return FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_ADMIN);
                });
                if($allowed) {
                    $controller->handleGetInfo(Container::_route()->match['id'], Container::_route()->match['hook'], Container::_route()->match['from']);
                } else {
                    $controller->respondEmpty();
                }
            });
    }

    public function setupAccessControl() {
        // this is just a part for admin-api's check-allowed endpoint, may be removed again
        FeatureContainer::_accessManager()
                        ->ability('hook.api')
                        ->grant([
                            AccessManager::ROLE_MANAGER,
                        ], [
                            new AccessAuthSource(static function($auth_source) {
                                // allow flood-admin as auth source by default
                                return 'flood-admin' === $auth_source;
                            }),
                        ]);

        // this is the default access scheme for the admin, a manager is enabled to do everything
        // overwrite with `ability('hook.api.view', 'hook-id')` for one hook
        // or bind also completely to an hook with `hookBindAbility('hook-id', 'hook.api.view')`
        // one of those is needed for the admin view, there is no default-fallback to non-hook abilities in admin but in PHP (atm.)
        // todo: this should be something like an template, e.g. the problem is the fallback to non-hooks for e.g. `listAll`, scene: 2 hook's, both has content sections, for one the manager is allowed for the other not, but the one where he is allowed is defined through `bindHookAblity` and this template exists, and the hook_context is false in the api, many methods will list everything and not only for one hook_context; there now is restricted to acces without an hook_context, but that should be able to be defined in his own way like `master-root-manager-can-list-from-anything`
        FeatureContainer::_accessManager()
                        ->ability('hook.api.view')
                        ->grant([
                            AccessManager::ROLE_MANAGER,
                        ], [
                            Definition::ACTION_ACCESS_ADMIN,
                            Definition::ACTION_ACCESS_FEATURE_MESSAGE,
                            Definition::ACTION_ACCESS_FEATURE_CONTENT,
                            Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION,
                            Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE,
                            Definition::ACTION_ACCESS_FEATURE_SHOP,
                            Definition::ACTION_ACCESS_FEATURE_SHOP_SETTING,
                            Definition::ACTION_ACCESS_FEATURE_SHOP_ORDER,
                            Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP,
                            Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT,
                            Definition::ACTION_ACCESS_FEATURE_INVENTORY,
                            Definition::ACTION_ACCESS_FEATURE_INVENTORY_ADMIN,
                            Definition::ACTION_ACCESS_FEATURE_INVENTORY_READ,
                            Definition::ACTION_ACCESS_FEATURE_INVENTORY_ITEMS_EDIT,
                            Definition::ACTION_ACCESS_FEATURE_MARKETING,
                            Definition::ACTION_ACCESS_FEATURE_MARKETING_NEWSLETTER,
                            Definition::ACTION_ACCESS_FEATURE_MEDIA,
                            Definition::ACTION_ACCESS_FEATURE_USER,
                            Definition::ACTION_ACCESS_FEATURE_ANALYZE,
                            Definition::ACTION_ACCESS_FEATURE_BLOCKS,
                            Definition::ACTION_ACCESS_FEATURE_CONNECTIONS,
                        ]);

        // default needed user permission
        // allow CLI and Manager everything
        // allow authenticated users only do things for their own account, but not changing roles
        // allow anonym/unauthenticated users only to register them self
        FeatureContainer::_accessManager()
                        ->ability('feature.user')
                        ->grant([
                            AccessManager::ENTITY_CLI,
                            AccessManager::ROLE_MANAGER,
                        ], [
                            AccessManager::ACTION_READ,
                            AccessManager::ACTION_CREATE,
                            AccessManager::ACTION_UPDATE,
                            AccessManager::ACTION_DELETE,
                            UserStorageAction\Definition::USER_CHANGE_NAME,
                            UserStorageAction\Definition::USER_CHANGE_PASS,
                            UserStorageAction\Definition::USER_CHANGE_ROLES,
                        ])
                        ->grant([
                            AccessManager::ENTITY_AUTHENTICATED,
                        ], [
                            AccessManager::ACTION_READ,
                            AccessManager::ACTION_UPDATE,
                            AccessManager::ACTION_DELETE,
                            UserStorageAction\Definition::USER_CHANGE_NAME,
                            UserStorageAction\Definition::USER_CHANGE_PASS,
                        ], static function($id) {
                            // allow an authenticated user to do actions only for his own account
                            return FeatureContainer::_userActive()->getId() === $id;
                        })
                        ->grant([
                            AccessManager::ENTITY_ANONYM,
                        ], [
                            AccessManager::ACTION_CREATE,
                        ]);
    }
}
