<?php

namespace HydroApi\Controller\User;

use Hydro\Input\Receive;
use HydroApi\Action\Definition;
use HydroApi\BaseController\BaseController;
use HydroApi\Lib\HookInfo;
use HydroFeature\Container as FeatureContainer;

class Auth extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    /**
     * Handles Login Verification
     */
    public function handleVerify() {
        $username = $this->receive->get('username', FILTER_SANITIZE_STRING);
        $pass = $this->receive->get('password', FILTER_SANITIZE_STRING);

        if(false !== $username && false !== $pass) {
            /**
             * @var \HydroFeature\User\StorageItem\User $user
             */
            $user = FeatureContainer::_user()->getByName(trim($username));
            if($user && $user->getPass() && $user->getActive()) {

                if(FeatureContainer::_user()->verify(trim($pass), $user->getPass())) {

                    $token = FeatureContainer::_userToken()::create($user->getId(), $user->getName(), $user->getRoles());
                    if($token) {
                        $this->resp_data = ['token' => $token];
                    } else {
                        $this->resp_data = ['error' => 'token-not-generated'];
                    }
                } else {
                    $this->resp_data = ['error' => 'user-not-verified'];
                }
            }
        }
        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'not-valid'];
        }

        $this->respondJson();
    }

    public function handleCheckAllowed() {
        $token = FeatureContainer::_userToken()::extractFromHeader();

        if($token) {
            // authenticated
            $this->resp_data = ['success' => true,];

            // now do authorization
            $auth_source = false;
            if($this->receive->has('authSource')) {
                $auth_source = $this->receive->get('authSource', FILTER_SANITIZE_STRING);
            }
            $requests = false;
            if($this->receive->has('requests')) {
                $requests = $this->receive->get('requests', FILTER_REQUIRE_ARRAY);
            }

            $this->resp_data['rules'] = [];

            if($auth_source) {
                $this->resp_data['rules']['admin'] = FeatureContainer::_accessManager()->isAllowed('hook.api', Definition::ACTION_ACCESS_AUTH_SOURCE, $auth_source);
            }

            $this->resp_data['rules']['user'] = FeatureContainer::_accessManager()->getAllowed('feature.user', false, true);

            $this->resp_data['rules']['view'] = FeatureContainer::_accessManager()->getAllowed('hook.api.view', false, true);

            $api_hooks = HookInfo::getApiEnabled();
            foreach($api_hooks as $hook) {
                $this->resp_data['rules']['hooks'][$hook['id']]['view'] = FeatureContainer::_accessManager()->getAllowed('hook.api.view', $hook['id'], true);
            }
        } else {
            if(FeatureContainer::_userToken()::isBearerInHeader()) {
                // bearer exist but not valid: login expired (unofficial, used by e.g. IIS)
                $this->addStatusHeader(440);
            } else {
                $this->addStatusHeader(401);
            }
            $this->resp_data = ['error' => 'not-valid'];
        }

        $this->respondJson();
    }

    public function handleLogout() {
        // JWT
        // nothing to do, token is automatically invalid and must be deleted from client not server
        $this->resp_data = ['success' => true];


        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'not-valid'];
        }

        $this->respondJson();
    }
}