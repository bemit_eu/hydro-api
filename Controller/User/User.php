<?php

namespace HydroApi\Controller\User;

use Hydro\Input\Receive;
use HydroApi\Action\Definition;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\User\AccessManager\Exception\NotAllowed;

class User extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleListRoles() {
        $this->resp_data = FeatureContainer::_userRoles()::getList();

        $this->respondJson();
    }

    public function handleList() {
        if(!$this->needHookContext()) {
            return;
        }

        $condition = [];

        $list = FeatureContainer::_user()->getUsers($condition);
        if(is_array($list)) {
            $this->resp_data = $list;
        } else {
            $this->resp_data = ['error' => 'users-not-read'];
        }

        $this->respondJson();
    }

    public function handleCreate() {
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);

        if(false !== $name) {
            $name = trim($name);

            if(0 === strlen($name)) {
                $this->addStatusHeader(400);
                $this->resp_data = ['error' => 'name-is-empty'];
            } else {
                $pass = $this->receive->get('password', FILTER_SANITIZE_STRING);
                if($pass && '' !== trim($pass)) {
                    $pass = trim($pass);
                } else {
                    $pass = false;
                }

                $roles = $this->receive->get('roles', FILTER_REQUIRE_ARRAY);
                if(!$roles) {
                    $roles = [];
                }
                $active = $this->receive->get('active', FILTER_SANITIZE_STRING) ? 1 : 0;

                try {
                    $created = FeatureContainer::_user()->create($name, $pass, $roles, $active);
                    if($created) {
                        $this->resp_data = ['success' => $created];
                    }
                } catch(NotAllowed $e) {
                    $this->addStatusHeader(403);
                    $this->resp_data = ['error' => 'not-allowed'];
                }
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'user-not-created'];
        }

        $this->respondJson();
    }

    public function handleUpdate($id) {
        $name = null;
        if($this->receive->has('name')) {
            $name = $this->receive->get('name', FILTER_SANITIZE_STRING);
        }
        $pass = null;
        if($this->receive->has('password')) {
            $pass = $this->receive->get('password', FILTER_SANITIZE_STRING);
        }

        $roles = null;
        if($this->receive->has('roles')) {
            $roles = $this->receive->get('roles', FILTER_REQUIRE_ARRAY);
            if(!$roles) {
                $roles = [];
            }
        }

        $active = null;
        if($this->receive->has('active')) {
            $active = $this->receive->get('active', FILTER_SANITIZE_STRING) ? 1 : 0;
        }


        if((false !== $name || false !== $pass || false !== $roles)) {
            $user = FeatureContainer::_user()->getById($id);
            if($user) {
                try {
                    if($name) {
                        $user->setName($name);
                    }
                    if($pass) {
                        $user->setPass($pass);
                    }
                    if(null !== $roles) {
                        $user->setRoles($roles);
                    }
                    if(null !== $active) {
                        $user->setActive($active);
                    }

                    if($user->save()) {
                        $this->resp_data = ['success' => $id];
                    }
                } catch(NotAllowed $e) {
                    $this->resp_data = ['error' => 'not-allowed'];
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'user-to-update-not-found'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'set-nothing-to-update'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'user-not-updated'];
        }

        $this->respondJson();
    }

    public function handleDelete($id) {
        $user = FeatureContainer::_user()->getById($id);

        try {
            if($user && $user->delete()) {
                $this->resp_data = ['success' => true];
            }
        } catch(NotAllowed $e) {
            $this->resp_data = ['error' => 'not-allowed'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'user-not-deleted'];
        }

        $this->respondJson();
    }

    public function handleGet($id) {
        $user = FeatureContainer::_user()->getById($id);

        if($user) {
            $this->resp_data = $user->export();
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'user-not-found'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'user-not-read'];
        }

        $this->respondJson();
    }
}