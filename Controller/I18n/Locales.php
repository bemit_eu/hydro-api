<?php

namespace HydroApi\Controller\I18n;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use Hydro\Container;
use HydroApi\HookHydroApi;

class Locales extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleList() {
        if(!$this->needHookContext()) {
            return;
        }

        $hook = Container::_hookStorage()->get($this->hook_context);
        if($hook) {
            $this->resp_data = $hook->getRouteLocaleList();
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'locales-not-read'];
        }

        $this->respondJson();
    }
}