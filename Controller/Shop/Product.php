<?php

namespace HydroApi\Controller\Shop;

use Hydro\Input\Receive;
use HydroApi\Action\Definition;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Product extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleCreate() {
        if(!$this->needHookContext()) {
            return;
        }
        $group = $this->receive->get('group', FILTER_SANITIZE_STRING);
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);
        $product_type = $this->receive->get('product_type', FILTER_SANITIZE_STRING);


        if(false !== $group && false !== $name && false !== $product_type) {
            $shop = FeatureContainer::_shop();
            $product_id = $shop->createProduct($group, $name, $product_type, $this->hook_context);

            if($product_id) {
                $this->resp_data = ['success' => $product_id];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'product-not-created'];
        }

        $this->respondJson();
    }

    public function handleUpdate($product_id) {
        $group = null;
        if($this->receive->has('group')) {
            $group = $this->receive->get('group', FILTER_SANITIZE_STRING);
        }
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);
        $product_type = $this->receive->get('product_type', FILTER_SANITIZE_STRING);

        if(
            (false !== $name || false !== $product_type || false !== $group)
            && $name
        ) {
            $shop = FeatureContainer::_shop();
            $product = $shop->getProduct($product_id);
            if($product) {
                // check if the selected product is in a hook the user is allowed to access
                $allowed = $this->accessAllowed(static function() use ($product) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $product->getHook()) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT, null, false, $product->getHook())
                    );
                });
                if($allowed) {
                    if($group) {
                        try {
                            $product->setGroup($group);
                        } catch(\Exception $e) {
                            $this->resp_data = ['error' => 'shop-product-update--group-not-valid'];
                        }
                    }
                    try {
                        $product->setName($name);
                    } catch(\Exception $e) {
                        $this->resp_data = ['error' => 'shop-product-update--name-not-valid'];
                    }

                    $product->setType($product_type);

                    if($product->save()) {
                        $this->resp_data = ['success' => $product_id];
                    } else {
                        $this->addStatusHeader(500);
                        $this->resp_data = ['error' => 'shop-product-update-save-failed'];
                    }
                } else {
                    $this->respondEmpty();
                    return;
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'shop-product-not-found'];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'product-not-updated'];
        }

        $this->respondJson();
    }

    public function handleGetOne($id) {
        $shop = FeatureContainer::_shop();
        $product = $shop->getProduct($id);
        if($product) {
            // check if the selected product is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($product) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $product->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT, null, false, $product->getHook())
                );
            });
            if($allowed) {
                $this->resp_data = $product->exportData();
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'product-not-found'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'product-not-read'];
        }

        $this->respondJson();
    }

    public function handleGetByProductNo($locale, $stock = false) {
        $product_no = $this->receive->get('products', FILTER_REQUIRE_ARRAY);
        if(is_string($product_no)) {
            $product_no = [$product_no];
        }

        $products = FeatureContainer::_shop()->getProductByProductNo($locale, $product_no, $stock);
        $ret_products = [];
        if($products) {
            foreach($products as $product) {
                $allowed = $this->accessAllowed(static function() use ($product) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $product->hook) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT, null, false, $product->hook)
                    );
                });
                if($allowed) {
                    // todo: get bundle data/bundled-articles/variations
                    $ret_products[$product->getProductNo()] = $product->exportData();
                } else {
                    $this->respondEmpty();
                    return;
                }
            }
        }
        $this->resp_data = $ret_products;

        $this->respondJson();
    }

    public function handleList() {
        if(!$this->needHookContext()) {
            return;
        }
        $shop = FeatureContainer::_shop();
        $products = $shop->getProducts($this->hook_context);
        if(is_array($products)) {
            $this->resp_data = $products;
        } else {
            $this->resp_data = ['error' => 'products-not-read'];
        }

        $this->respondJson();
    }

    public function handleDelete($id) {
        $shop = FeatureContainer::_shop();
        $product = $shop->getProduct($id);
        if($product) {
            // check if the selected product is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($product) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $product->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT, null, false, $product->getHook())
                );
            });
            if($allowed) {
                if($product->delete()) {
                    $this->resp_data = ['success' => true];
                }
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'shop-product-not-found'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'product-not-deleted'];
        }

        $this->respondJson();
    }

    public function handleAddLocale($product_id, $locale) {
        if(!empty($product_id) && !empty($locale)) {
            $product = FeatureContainer::_shop()->getProduct($product_id);
            if($product) {
                // check if the selected article is in an hook the user is allowed to access
                $allowed = $this->accessAllowed(static function() use ($product) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $product->getHook()) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT, null, false, $product->getHook())
                    );
                });
                if($allowed) {
                    $inserted = FeatureContainer::_shop()->createProductData($product_id, $locale);
                    if($inserted) {
                        $this->resp_data = ['success' => $inserted];
                    }
                } else {
                    $this->respondEmpty();
                    return;
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'shop-product-of-data-not-found'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'product-and-locale-needed'];
        }

        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'locale-not-added'];
        }

        $this->respondJson();
    }

    public function handleUpdateData($data_id) {
        $product_no = $this->receive->get('product_no', FILTER_SANITIZE_STRING);
        $active = $this->receive->get('active', FILTER_VALIDATE_BOOLEAN);
        $hidden = $this->receive->get('hidden', FILTER_VALIDATE_BOOLEAN);
        $buy_able = $this->receive->get('buy_able', FILTER_VALIDATE_BOOLEAN);
        $tax_group = $this->receive->get('tax_group', FILTER_SANITIZE_STRING);
        $price = $this->receive->get('price', FILTER_SANITIZE_NUMBER_INT);
        $bundle = $this->receive->get('bundle');
        $data = null;
        if($this->receive->has('data')) {
            $data = $this->receive->get('data', FILTER_REQUIRE_ARRAY);
        }

        if(
            !empty($data_id) &&
            (null !== $active && null !== $hidden && null !== $buy_able) &&
            (false !== $tax_group && false !== $price && false !== $data)
        ) {
            $shop = FeatureContainer::_shop();
            $product_data = $shop->getProductData($data_id);
            if($product_data) {
                $product = FeatureContainer::_shop()->getProduct($product_data->getProduct());
                if($product) {
                    // check if the selected article is in an hook the user is allowed to access
                    $allowed = $this->accessAllowed(static function() use ($product) {
                        return (
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $product->getHook()) &&
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT, null, false, $product->getHook())
                        );
                    });
                    if($allowed) {
                        $product_data->setProductNo($product_no);
                        $product_data->setActive($active);
                        $product_data->setHidden($hidden);
                        $product_data->setBuyAble($buy_able);
                        $product_data->setTaxGroup($tax_group);
                        $product_data->setPrice($price);
                        $product_data->setBundle($bundle);
                        $product_data->setData($data);
                        if($product_data->save()) {
                            $this->resp_data = ['success' => true];
                        } else {
                            $this->addStatusHeader(500);
                            $this->resp_data = ['error' => 'shop-product-data-not-updated'];
                        }
                    } else {
                        $this->respondEmpty();
                        return;
                    }
                } else {
                    $this->addStatusHeader(404);
                    $this->resp_data = ['error' => 'shop-product-of-data-not-found'];
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'product-data-not-found'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => []];
            if(null === $active) {
                $this->resp_data['error'][] = 'active-is-invalid';
            }
            if(null === $hidden) {
                $this->resp_data['error'][] = 'hidden-is-invalid';
            }
            if(null === $buy_able) {
                $this->resp_data['error'][] = 'buy_able-is-invalid';
            }
            if(false === $tax_group) {
                $this->resp_data['error'][] = 'tex_group-is-invalid';
            }
            if(false === $price) {
                $this->resp_data['error'][] = 'price-is-invalid';
            }
            if(false === $data) {
                $this->resp_data['error'][] = 'data-is-invalid';
            }
        }

        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'product-data-not-updated'];
        }

        $this->respondJson();
    }

    public function handleDeleteLocale($data_id) {
        if(!empty($data_id)) {
            $shop = FeatureContainer::_shop();
            $product_data = $shop->getProductData($data_id);

            if($product_data) {
                $product = FeatureContainer::_shop()->getProduct($product_data->getProduct());
                if($product) {
                    // check if the selected article is in an hook the user is allowed to access
                    $allowed = $this->accessAllowed(static function() use ($product) {
                        return (
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $product->getHook()) &&
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_PRODUCT, null, false, $product->getHook())
                        );
                    });
                    if($allowed) {
                        if($product_data->delete()) {
                            $this->resp_data = ['success' => true];
                        } else {
                            $this->addStatusHeader(500);
                            $this->resp_data = ['error' => 'shop-product-data-not-deleted'];
                        }
                    } else {
                        $this->respondEmpty();
                        return;
                    }
                } else {
                    $this->addStatusHeader(404);
                    $this->resp_data = ['error' => 'shop-product-of-data-not-found'];
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'shop-product-data-not-found'];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'locale-not-deleted'];
        }

        $this->respondJson();
    }
}