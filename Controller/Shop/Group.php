<?php

namespace HydroApi\Controller\Shop;

use Hydro\Input\Receive;
use HydroApi\Action\Definition;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Group extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleCreate() {
        if(!$this->needHookContext()) {
            return;
        }
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);

        if(false !== $name) {
            $group_id = FeatureContainer::_shop()->createGroup($name, $this->hook_context);
            if($group_id) {
                $this->resp_data = ['success' => $group_id];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'group-not-created'];
        }

        $this->respondJson();
    }

    public function handleList() {
        if(!$this->needHookContext()) {
            return;
        }
        $shop = FeatureContainer::_shop();
        $groups = $shop->getGroups($this->hook_context);
        if($groups || [] === $groups) {
            $this->resp_data = $groups;
        } else {
            $this->resp_data = ['error' => 'groups-not-read'];
        }

        $this->respondJson();
    }

    public function handleGet($id) {
        $content = FeatureContainer::_shop();
        $group = $content->getGroup($id);

        if($group) {
            // check if the selected group is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($group) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $group->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP, null, false, $group->getHook())
                );
            });
            if($allowed) {
                $this->resp_data = [
                    'id'   => $group->getId(),
                    'name' => $group->getName(),
                    'hook' => $group->getHook(),
                ];
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'shop-group-not-found'];
        }

        $this->respondJson();
    }

    public function handleUpdate($id) {
        if(!$this->needHookContext()) {
            return;
        }
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);

        if(false !== $name && 0 < strlen($name)) {

            $group = FeatureContainer::_shop()->getGroup($id);
            if($group) {
                // check if the selected group is in a hook the user is allowed to access
                $allowed = $this->accessAllowed(static function() use ($group) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $group->getHook()) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP, null, false, $group->getHook())
                    );
                });
                if($allowed) {
                    $group->setName($name);
                    if($group->save()) {
                        $this->resp_data = ['success' => $group->getId()];
                    } else {
                        $this->resp_data = ['error' => 'group-update-failed'];
                    }
                } else {
                    $this->respondEmpty();
                    return;
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'shop-group-not-found'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'group-name-is-empty'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'group-not-created'];
        }

        $this->respondJson();
    }

    public function handleDelete($id) {
        $group = FeatureContainer::_shop()->getGroup($id);
        if($group) {
            // check if the selected group is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($group) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $group->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP, null, false, $group->getHook())
                );
            });
            if($allowed) {
                if($group->delete()) {
                    $this->resp_data = ['success' => true];
                } else {
                    $this->addStatusHeader(500);
                    $this->resp_data = ['error' => 'delete-failed'];
                }
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'shop-group-not-found'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'group-not-deleted'];
        }

        $this->respondJson();
    }

    public function handleProductList($id) {
        $shop = FeatureContainer::_shop();
        $group = $shop->getGroup($id);
        $set = false;
        if($group) {
            // check if the selected group is in an hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($group) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $group->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_GROUP, null, false, $group->getHook())
                );
            });
            if($allowed) {
                $storageProducts = $group->getProducts();
                if(is_array($storageProducts)) {
                    $set = true;
                    $this->resp_data = $storageProducts;
                }
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'shop-group-not-found'];
        }

        if(!$set && empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'group-storageProducts-not-read'];
        }

        $this->respondJson();
    }
}