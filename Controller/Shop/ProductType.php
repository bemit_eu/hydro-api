<?php

namespace HydroApi\Controller\Shop;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class ProductType extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleGetOne($id) {
        $product_type = FeatureContainer::_shopProductType()::get($id, $this->hook_context);
        if(!$product_type) {
            $product_type = FeatureContainer::_shopProductType()::get($id);
        }
        if($product_type) {
            $this->resp_data = $product_type;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'product-type-not-read'];
        }

        $this->respondJson();
    }

    public function handleList() {
        $list = FeatureContainer::_shopProductType()::getListMerged($this->hook_context);
        if($list) {
            $this->resp_data = $list;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'product-types-not-read'];
        }

        $this->respondJson();
    }
}