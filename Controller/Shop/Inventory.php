<?php

namespace HydroApi\Controller\Shop;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Inventory extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleListWarehouses() {
        $list = FeatureContainer::_inventory()->getWarehouses();
        if($list) {
            $this->resp_data = $list;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'warehouses-not-read'];
        }

        $this->respondJson();
    }

    public function handleListStocks() {
        $list = FeatureContainer::_inventory()->getStocks();
        if($list) {
            $this->resp_data = $list;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'stocks-not-read'];
        }

        $this->respondJson();
    }

    public function handleListStockItemsOfStock($stock) {
        $list = FeatureContainer::_inventory()->getStockItems($stock);
        if(is_array($list)) {
            $this->resp_data = $list;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'stock-items-not-read'];
        }

        $this->respondJson();
    }

    public function handleListStockItems() {
        $list = FeatureContainer::_inventory()->getStockItemsDetailed();
        if(is_array($list)) {
            $this->resp_data = $list;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'stock-items-not-read'];
        }

        $this->respondJson();
    }

    public function handleCreateItem() {
        $stock = $this->receive->get('stock', FILTER_SANITIZE_STRING);
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);
        $sku = $this->receive->get('sku', FILTER_SANITIZE_STRING);

        $products = $this->receive->get('products', FILTER_REQUIRE_ARRAY);
        $amount_available = $this->receive->get('amount_available', FILTER_SANITIZE_NUMBER_INT);

        if(false !== $stock && false !== $name) {
            $stock_item_id = FeatureContainer::_inventory()->createStockItem($stock, $name, (!$sku ?: $sku), (!$amount_available ?: $amount_available));
            if($stock_item_id) {
                if($products) {
                    FeatureContainer::_inventory()->bindProducts2StockItem($products, $stock_item_id);
                }
                $this->resp_data = ['success' => $stock_item_id];
            } else {
                $this->addStatusHeader(500);
                $this->resp_data = ['error' => 'stock-items-not-created'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'stock-items-create-data-invalid'];
        }

        $this->respondJson();
    }

    public function handleUpdateItemQuantity($item) {
        $available_add = $this->receive->get('available_add', FILTER_SANITIZE_NUMBER_INT);
        $available_remove = $this->receive->get('available_remove', FILTER_SANITIZE_NUMBER_INT);
        $reserve = $this->receive->get('available_reserve', FILTER_SANITIZE_NUMBER_INT);
        $reserved_remove = $this->receive->get('reserved_remove', FILTER_SANITIZE_NUMBER_INT);
        $reserved_unreserve = $this->receive->get('reserved_unreserve', FILTER_SANITIZE_NUMBER_INT);

        if(
            false !== $available_add ||
            false !== $available_remove ||
            false !== $reserve ||
            false !== $reserved_remove ||
            false !== $reserved_unreserve
        ) {
            $this->resp_data = [];
            $changed = false;
            if($available_add) {
                $update_success = FeatureContainer::_inventory()->stockItemAvailableAdd($item, $available_add);
                if(false !== $update_success) {
                    $this->resp_data['available_add'] = ['success' => $update_success];
                    $changed = true;
                } else {
                    $this->resp_data['available_add'] = ['error' => 'stock-item-qty-not-changed'];
                }
            }
            if($available_remove) {
                $update_success = FeatureContainer::_inventory()->stockItemAvailableRemove($item, $available_remove);
                if(false !== $update_success) {
                    $this->resp_data['available_remove'] = ['success' => $update_success];
                    $changed = true;
                } else {
                    $this->resp_data['available_remove'] = ['error' => 'stock-item-qty-not-changed'];
                }
            }
            if($reserve) {
                $update_success = FeatureContainer::_inventory()->stockItemReserve($item, $reserve);
                if(false !== $update_success) {
                    $this->resp_data['reserve'] = ['success' => $update_success];
                    $changed = true;
                } else {
                    $this->resp_data['reserve'] = ['error' => 'stock-item-qty-not-changed'];
                }
            }
            if($reserved_remove) {
                $update_success = FeatureContainer::_inventory()->stockItemReservedRemove($item, $reserved_remove);
                if(false !== $update_success) {
                    $this->resp_data['reserved_remove'] = ['success' => $update_success];
                    $changed = true;
                } else {
                    $this->resp_data['reserved_remove'] = ['error' => 'stock-item-qty-not-changed'];
                }
            }
            if($reserved_unreserve) {
                $update_success = FeatureContainer::_inventory()->stockItemUnreserve($item, $reserved_unreserve);
                if(false !== $update_success) {
                    $this->resp_data['unreserve'] = ['success' => $update_success];
                    $changed = true;
                } else {
                    $this->resp_data['unreserve'] = ['error' => 'stock-item-qty-not-changed'];
                }
            }

            if($changed) {
                $stock_item = FeatureContainer::_inventory()->getStockItem($item, true);
                if(false !== $stock_item) {
                    $this->resp_data['stock_item'] = $stock_item;
                } else {
                    $this->resp_data['stock_item'] = false;
                }
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'stock-items-change-qty-data-invalid'];
        }

        $this->respondJson();
    }

    public function handleGetItemByProduct($product) {
        $stock_item = FeatureContainer::_inventory()->getStockItemForProduct($product);

        if($stock_item) {
            $id = $stock_item['ID'];
            unset($stock_item['ID']);
            $stock_item['id'] = $id;
            $this->resp_data = $stock_item;
        } else {
            $product_storage = new \HydroFeature\Shop\StorageAbstraction\Product(false);
            $product = $product_storage->getProductData($product, true);
            if($product_storage->isBundle($product)) {
                // handle stats for bundled products
                $this->resp_data = [
                    'products' => $product->getData()['products'],
                    'bundled' => [],
                    'bundle_info' => $product->getData()['bundle']['available'],
                ];

                foreach($product->getData()['bundled'] as $bundled) {
                    /**
                     * @var \HydroFeature\Shop\StorageItem\ProductData $bundled
                     */
                    $this->resp_data['bundled'][$bundled->getProductNo()] = [
                        'amount_available' => $bundled->amount_available,
                        'amount_reserved' => $bundled->amount_reserved,
                        'stock_item' => $bundled->stock_item,
                    ];
                }
            } else {
                $this->resp_data = false;
            }
        }

        $this->respondJson();
    }
}