<?php

namespace HydroApi\Controller\Shop;

use Hydro\Input\Receive;
use HydroApi\Action\Definition;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Order extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleList($state) {
        if(!$this->needHookContext()) {
            return;
        }

        $condition = [];

        if($state && 'all' !== $state && filter_var($state, FILTER_SANITIZE_STRING)) {
            if('none' === $state) {
                $condition['state'] = null;
            } else {
                $condition['state'] = $state;
            }
        }

        $hooks = $this->receive->get('hooks', FILTER_REQUIRE_ARRAY);

        if($hooks && !empty($hooks)) {
            $condition['OR'] = [];
            foreach($hooks as $k => $hook) {
                if(0 < strlen($hook)) {
                    $allowed = $this->accessAllowed(static function() use ($hook) {
                        return (
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP, null, false, $hook) &&
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_SHOP_ORDER, null, false, $hook)
                        );
                    });
                    if($allowed) {
                        $condition['OR']['hook#' . $k] = $hook;
                    } else {
                        $this->respondEmpty();
                        return;
                    }
                }
            }
        }

        /*$condition = [
            'limit' => 100,
        ];
        if($this->receive->has('limit')) {
            $condition['limit'] = $this->receive->get('limit');
        }*/
        $list = FeatureContainer::_shop()->getOrders($condition);
        if(is_array($list)) {
            $this->resp_data = $list;
        } else {
            $this->resp_data = ['error' => 'orders-not-read'];
        }

        $this->respondJson();
    }

    public function handleUpdate($data_id) {
        $state = $this->receive->get('state', FILTER_SANITIZE_STRING);

        if(
            !empty($data_id) &&
            (false !== $state)
        ) {
            $shop = FeatureContainer::_shop();
            $order = $shop->getOrder($data_id);
            // todo: check if order is in allowed hook
            if($order) {
                if(!is_array($order->state_log)) {
                    $order->state_log = [];
                }
                $from = $order->state ? $order->state : 'none';
                $order->state_log[] = [
                    'time' => time(),
                    'from' => $from,
                    'to' => $state,
                ];

                //FeatureContainer::_workflow()->trigger('order')->next($from, $state, $order);
                // try/catch: not-valid
                // try/catch: not-found
                // try/catch: failed

                $order->state = $state;
                $storage = new \HydroFeature\Shop\StorageHandler\Order();
                $updated = $storage->update($data_id, [
                    'state' => $order->state,
                    'state_log' => json_encode($order->state_log),
                ]);
                if(is_int($updated)) {
                    $this->resp_data = ['success' => true];
                }
            } else {
                $this->resp_data = ['error' => 'order-not-found-for-update'];
            }
        } else {
            $this->resp_data = ['error' => []];
            if(false === $state) {
                $this->resp_data['error'][] = 'state-is-invalid';
            }
        }

        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'order-not-updated'];
        }

        $this->respondJson();
    }

    public function handleGet($data_id) {
        if(!empty($data_id)) {
            $shop = FeatureContainer::_shop();
            $order = $shop->getOrder($data_id);
            // todo: check if order is in allowed hook
            if($order) {
                $order->id = $data_id;
                $this->resp_data = $order;
            }
        }

        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'order-not-found'];
        }

        $this->respondJson();
    }
}
