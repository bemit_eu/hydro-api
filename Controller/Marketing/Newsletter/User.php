<?php

namespace HydroApi\Controller\Marketing\Newsletter;

use Hydro\Container;
use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\Newsletter\NewsletterRegistrationHandler;
use HydroFeature\Newsletter\StorageHandler\NewsletterUser as StorageNewsletterUser;
use HydroFeature\Newsletter\StorageItem\NewsletterUser;

class User extends BaseController {
    protected $receive;
    protected $storage;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
        $this->storage = new StorageNewsletterUser();
    }

    public function handleList() {
        $condition = [];
        /*$condition = [
            'limit' => 100,
        ];
        if($this->receive->has('limit')) {
            $condition['limit'] = $this->receive->get('limit');
        }*/
        $list = FeatureContainer::_newsletter()->getUsers($condition);
        if(is_array($list)) {
            $this->resp_data = $list;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'users-not-read'];
        }

        $this->respondJson();
    }

    public function handleCreate() {
        if(!$this->needHookContext()) {
            return;
        }

        $email = $this->receive->get('email', FILTER_SANITIZE_STRING);
        $optin = $this->receive->get('optin', FILTER_SANITIZE_STRING);
        $emails = $this->receive->get('emails', FILTER_REQUIRE_ARRAY);

        if($email) {
            $inserted = $this->saveOneUser($email, $optin);
            if($inserted) {
                $this->resp_data = ['success' => $inserted];
            } else {
                $this->addStatusHeader(500);
                $this->resp_data = ['error' => 'user-not-saved'];
            }
        } else if($emails) {
            $inserted = [];
            $failed = [];
            if(is_array($emails)) {
                foreach($emails as $recipient_raw) {
                    $recipient = explode(',', $recipient_raw);
                    $email = false;
                    $optin = false;
                    if(isset($recipient[0])) {
                        $email = trim($recipient[0]);
                    }
                    if(isset($recipient[1])) {
                        $optin = trim($recipient[1]);
                    }
                    if(0 < strlen($email)) {
                        $ins = $this->saveOneUser($email, $optin);
                        if($ins) {
                            $inserted[] = [$email => $ins];
                        } else {
                            $failed[] = $email;
                        }
                    } else {
                        $failed[] = 'empty-mail-found';
                    }
                }
                $this->resp_data = ['success' => $inserted];

                if(!empty($failed)) {
                    $this->resp_data['failed'] = $failed;
                }
            } else {
                $this->addStatusHeader(400);
                $this->resp_data = ['error' => 'users-wrong-format'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'user-or-users-empty'];
        }

        $this->respondJson();
    }

    protected function saveOneUser($email, $optin = false) {
        $email = trim($email);
        if($this->storage->getByEmail($email)) {
            return 'already-exists';
        } else {
            $user = new NewsletterUser();
            $user->email = $email;
            $user->state = NewsletterRegistrationHandler::SUBSCRIBED;
            $user->subscribing_time = date('Y-m-d H:i:s');
            $user->optin_time = date('Y-m-d H:i:s');
            $user->token = Container::_storageData()->randomId();

            $user->hook = $this->hook_context;
            $user->source = NewsletterUser::SOURCE_MANUAL;

            if($optin) {
                $user->optin_reference = trim($optin);
            }
            $inserted = $this->storage->create($user);
            if($inserted) {
                return $inserted;
            }
        }
        return false;
    }
}