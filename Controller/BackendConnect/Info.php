<?php

namespace HydroApi\Controller\BackendConnect;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use Hydro\Container;
use HydroApi\HookHydroApi;
use HydroApi\Lib\HookInfo;

class Info extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleList() {
        $this->resp_data = [
            'hooks' => HookInfo::getApiEnabled(),
        ];

        /*$hook = Container::_hookStorage()->get($this->hook_context);
        if($hook) {
            $this->resp_data = $hook->getRouteLocaleList();
        }*/

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'backend-no-hooks'];
        }

        $this->respondJson();
    }
}