<?php

namespace HydroApi\Controller\Workflow;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Workflow extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleGetInfo($id, $hook, $from, $to = false) {
        $this->resp_data = FeatureContainer::_workflow()->inspect($id, $hook)->inspect($from);


        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'not-valid'];
        }

        $this->respondJson();
    }
}