<?php

namespace HydroApi\Controller\Content;

use Flood\Component\Route\Container as RouteContainer;
use Hydro\Input\Receive;
use HydroApi\Action\Definition;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Article extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleCreate() {
        if(!$this->needHookContext()) {
            return;
        }
        $section = $this->receive->get('section', FILTER_SANITIZE_STRING);
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);

        $tag = null;
        if($this->receive->has('tag')) {
            $tag = $this->receive->get('tag', FILTER_SANITIZE_STRING);
        }

        if(false !== $section && false !== $name && false !== $tag) {
            $content = FeatureContainer::_content();
            $article_id = $content->createArticle($section, $name, $this->hook_context, $tag);

            if($article_id) {
                $this->resp_data = ['success' => $article_id];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'article-not-created'];
        }

        $this->respondJson();
    }

    public function handleUpdate($article_id) {
        $section = null;
        if($this->receive->has('section')) {
            // todo: here must be checked if the user has the right for sections and the section updating is in an hook he got access too
            $section = $this->receive->get('section', FILTER_SANITIZE_STRING);
        }
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);
        $tag = $this->receive->get('tag', FILTER_SANITIZE_STRING);

        if(
            (false !== $name || false !== $tag || false !== $section)
            && $name
        ) {
            $content = FeatureContainer::_content();
            $article = $content->getArticle($article_id);
            if($article) {
                // check if the selected article is in a hook the user is allowed to access
                $allowed = $this->accessAllowed(static function() use ($article) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $article->getHook()) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE, null, false, $article->getHook())
                    );
                });
                if($allowed) {
                    if($section) {
                        try {
                            $article->setSection($section);
                        } catch(\Exception $e) {
                            $this->addStatusHeader(400);
                            $this->resp_data = ['error' => 'section-not-valid'];
                        }
                    }
                    try {
                        $article->setName($name);
                    } catch(\Exception $e) {
                        $this->addStatusHeader(400);
                        $this->resp_data = ['error' => 'name-not-valid'];
                    }
                    $article->setTag($tag);

                    if($article && $article->save()) {
                        $this->resp_data = ['success' => $article_id];
                    }
                } else {
                    $this->respondEmpty();
                    return;
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'article-to-update-not-found'];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'article-not-updated'];
        }

        $this->respondJson();
    }

    public function handleGetOne($id) {
        $content = FeatureContainer::_content();
        $article = $content->getArticle($id);

        if($article) {
            // check if the selected article is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($article) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $article->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE, null, false, $article->getHook())
                );
            });
            if($allowed) {
                $this->resp_data = $article->exportData();
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'content-article-not-found'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'article-not-read'];
        }

        $this->respondJson();
    }

    public function handleList() {
        if(!$this->needHookContext()) {
            return;
        }
        $content = FeatureContainer::_content();
        $articles = $content->getArticles($this->hook_context);
        if($articles) {
            $this->resp_data = $articles;
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'articles-not-read'];
        }

        $this->respondJson();
    }

    public function handleDelete($id) {
        $content = FeatureContainer::_content();
        $article = $content->getArticle($id);
        if($article) {
            // check if the selected article is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($article) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $article->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE, null, false, $article->getHook())
                );
            });
            if($allowed) {
                if($article->delete()) {
                    $this->resp_data = ['success' => true];
                }
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'content-article-not-found'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'article-not-deleted'];
        }

        $this->respondJson();
    }

    public function handleAddLocale($article_id, $locale) {
        $page_type = $this->receive->get('page_type', FILTER_SANITIZE_STRING);
        if(!empty($article_id) && !empty($locale) && $page_type) {
            $article = FeatureContainer::_content()->getArticle($article_id);
            if($article) {
                // check if the selected article is in a hook the user is allowed to access
                $allowed = $this->accessAllowed(static function() use ($article) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $article->getHook()) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE, null, false, $article->getHook())
                    );
                });
                if($allowed) {
                    $inserted = FeatureContainer::_content()->createArticleData($article_id, $locale, $page_type, $article->getHook());
                    if($inserted) {
                        $this->resp_data = ['success' => $inserted];
                    }
                } else {
                    $this->respondEmpty();
                    return;
                }
            } else {
                $this->addStatusHeader(400);
                $this->resp_data = ['error' => 'article-of-data-not-found'];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'locale-not-added'];
        }

        $this->respondJson();
    }

    public function handleUpdateLocale($data_id) {
        $meta = $this->receive->get('meta', FILTER_REQUIRE_ARRAY);
        $mainText = $this->receive->get('mainText', FILTER_UNSAFE_RAW);
        $docTree = $this->receive->get('docTree', FILTER_REQUIRE_ARRAY);
        if(
            !empty($data_id) &&
            ($meta || $mainText || $docTree)
        ) {
            $content = FeatureContainer::_content();
            $article_data = $content->getArticleData($data_id);
            $article = FeatureContainer::_content()->getArticle($article_data->getArticle());
            if($article) {
                // check if the selected article is in a hook the user is allowed to access
                $allowed = $this->accessAllowed(static function() use ($article) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $article->getHook()) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE, null, false, $article->getHook())
                    );
                });
                if($allowed) {
                    $is_error = false;
                    if($article_data) {
                        try {
                            if($meta) {
                                $article_data->setMeta($meta);
                            }
                        } catch(\Exception $e) {
                            $is_error = true;
                            $this->addStatusHeader(400);
                            $this->resp_data = ['error' => 'invalid-data-meta'];
                        }
                        try {
                            if(!$is_error && $mainText) {
                                $article_data->setMainText($mainText);
                            }
                        } catch(\Exception $e) {
                            $is_error = true;
                            $this->addStatusHeader(400);
                            $this->resp_data = ['error' => 'invalid-data-main-text'];
                        }
                        try {
                            if(!$is_error && $docTree) {
                                $article_data->setDocTree($docTree);
                            }
                        } catch(\Exception $e) {
                            $is_error = true;
                            $this->addStatusHeader(400);
                            $this->resp_data = ['error' => 'invalid-data-doc-tree'];
                        }

                        if(!$is_error && $article_data->save()) {
                            $this->resp_data = ['success' => true];
                        }
                    } else {
                        $this->addStatusHeader(404);
                        $this->resp_data = ['error' => 'content-article-data-not-found'];
                    }
                } else {
                    $this->respondEmpty();
                    return;
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'article-of-data-not-found'];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'article-data-not-updated'];
        }

        $this->respondJson();
    }

    public function handleDeleteLocale($data_id) {
        if(!empty($data_id)) {
            $content = FeatureContainer::_content();
            $article_data = $content->getArticleData($data_id);

            if($article_data) {
                $article = FeatureContainer::_content()->getArticle($article_data->getArticle());
                if($article) {
                    // check if the selected article is in a hook the user is allowed to access
                    $allowed = $this->accessAllowed(static function() use ($article) {
                        return (
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $article->getHook()) &&
                            FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE, null, false, $article->getHook())
                        );
                    });
                    if($allowed) {
                        if($article_data->delete()) {
                            $this->resp_data = ['success' => true];
                        }
                    } else {
                        $this->respondEmpty();
                        return;
                    }
                } else {
                    $this->addStatusHeader(404);
                    $this->resp_data = ['error' => 'article-of-data-not-found'];
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'content-article-data-not-found'];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'locale-not-deleted'];
        }

        $this->respondJson();
    }
}