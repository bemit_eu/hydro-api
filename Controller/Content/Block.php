<?php

namespace HydroApi\Controller\Content;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Block extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleGetOne($id) {
        if(!$this->needHookContext()) {
            return;
        }

        $block = FeatureContainer::_contentBlock()::get($id, $this->hook_context);
        if(!$block) {
            // fallback to global is no block exist especially for the hook
            $block = FeatureContainer::_contentBlock()::get($id);
        }
        if($block) {
            $this->resp_data = $block;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'block-not-read'];
        }

        $this->respondJson();
    }

    public function handleList() {
        if(!$this->needHookContext()) {
            return;
        }

        $blocks = FeatureContainer::_contentBlock()::getListMerged($this->hook_context);
        if($blocks) {
            $this->resp_data = $blocks;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'blocks-not-read'];
        }

        $this->respondJson();
    }
}