<?php

namespace HydroApi\Controller\Content;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Schema extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleGetOne($id) {
        $schema = FeatureContainer::_contentSchema()::get($id, $this->hook_context);
        if(!$schema) {
            // fallback, when schema doesn't exist especially for hook, use global
            $schema = FeatureContainer::_contentSchema()::get($id);
        }

        if($schema) {
            $this->resp_data = $schema;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'schema-not-read'];
        }

        $this->respondJson();
    }
}