<?php

namespace HydroApi\Controller\Content;

use Hydro\Input\Receive;
use HydroApi\Action\Definition;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Section extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleCreate() {
        if(!$this->needHookContext()) {
            return;
        }
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);

        if(false !== $name) {
            $section_id = FeatureContainer::_content()->createSection($name, $this->hook_context);
            if($section_id) {
                $this->resp_data = ['success' => $section_id];
            }
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'section-not-created'];
        }

        $this->respondJson();
    }

    public function handleUpdate($id) {
        if(!$this->needHookContext()) {
            return;
        }
        $name = $this->receive->get('name', FILTER_SANITIZE_STRING);

        if(false !== $name && 0 < strlen($name)) {

            $section = FeatureContainer::_content()->getSection($id);
            if($section) {
                // check if the selected section is in a hook the user is allowed to access
                $allowed = $this->accessAllowed(static function() use ($section) {
                    return (
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $section->getHook()) &&
                        FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION, null, false, $section->getHook())
                    );
                });
                if($allowed) {
                    $section->setName($name);
                    if($section->save()) {
                        $this->resp_data = ['success' => $section->getId()];
                    } else {
                        $this->resp_data = ['error' => 'section-update-failed'];
                    }
                } else {
                    $this->respondEmpty();
                    return;
                }
            } else {
                $this->addStatusHeader(404);
                $this->resp_data = ['error' => 'content-section-not-found'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'section-name-is-empty'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'section-not-created'];
        }

        $this->respondJson();
    }

    public function handleDelete($id) {
        $content = FeatureContainer::_content();
        $section = $content->getSection($id);
        if($section) {
            // check if the selected section is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($section) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $section->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION, null, false, $section->getHook())
                );
            });
            if($allowed) {
                if($section->delete()) {
                    $this->resp_data = ['success' => true];
                } else {
                    $this->addStatusHeader(500);
                    $this->resp_data = ['error' => 'delete-failed'];
                }
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'content-section-not-found'];
        }

        if(empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'section-not-deleted'];
        }

        $this->respondJson();
    }

    public function handleList() {
        if(!$this->needHookContext()) {
            return;
        }
        $content = FeatureContainer::_content();
        $sections = $content->getSections($this->hook_context);
        if(is_array($sections)) {
            $this->resp_data = $sections;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'sections-not-read'];
        }

        $this->respondJson();
    }

    public function handleGet($id) {
        $content = FeatureContainer::_content();
        $section = $content->getSection($id);

        if($section) {
            // check if the selected section is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($section) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $section->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION, null, false, $section->getHook())
                );
            });
            if($allowed) {
                $this->resp_data = [
                    'id'   => $section->getId(),
                    'name' => $section->getName(),
                    'hook' => $section->getHook(),
                ];
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'content-section-not-found'];
        }

        $this->respondJson();
    }

    public function handleArticleList($id) {
        $content = FeatureContainer::_content();
        $section = $content->getSection($id);

        $set = false;
        if($section) {
            // check if the selected section is in a hook the user is allowed to access
            $allowed = $this->accessAllowed(static function() use ($section) {
                return (
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT, null, false, $section->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_ARTICLE, null, false, $section->getHook()) &&
                    FeatureContainer::_accessManager()->isAllowed('hook.api.view', Definition::ACTION_ACCESS_FEATURE_CONTENT_SECTION, null, false, $section->getHook())
                );
            });
            if($allowed) {
                $articles = $section->getArticles();
                if(is_array($articles)) {
                    $set = true;
                    $this->resp_data = $articles;
                }
            } else {
                $this->respondEmpty();
                return;
            }
        } else {
            $this->addStatusHeader(404);
            $this->resp_data = ['error' => 'content-section-not-found'];
        }

        if(!$set && empty($this->resp_data)) {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'section-articles-not-read'];
        }

        $this->respondJson();
    }
}