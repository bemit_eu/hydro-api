<?php

namespace HydroApi\Controller\Content;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class PageType extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function handleGetOne($id) {
        $pagetype = FeatureContainer::_contentPageType()::get($id, $this->hook_context);
        if(!$pagetype) {
            // fallback, when pagetype doesn't exist especially for hook, use global
            $pagetype = FeatureContainer::_contentPageType()::get($id);
        }

        if($pagetype) {
            $this->resp_data = $pagetype;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'pagetype-not-read'];
        }

        $this->respondJson();
    }

    public function handleList() {
        $list = FeatureContainer::_contentPageType()::getListMerged($this->hook_context);

        if($list) {
            $this->resp_data = $list;
        } else {
            $this->addStatusHeader(500);
            $this->resp_data = ['error' => 'pagetypes-not-read'];
        }

        $this->respondJson();
    }
}