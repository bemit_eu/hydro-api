<?php

namespace HydroApi\Controller\FileManager;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class Dir extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    public function list($dir) {
        $this->resp_data = [];

        $filem = FeatureContainer::_fileManager();
        try {
            $this->resp_data = $filem->list($dir);
        } catch(\Exception $e) {
            $this->resp_data = ['error' => 'access-not-allowed'];
        }

        $this->respondJson();
    }

    public function createDir($dir) {
        $this->resp_data = [];

        $filem = FeatureContainer::_fileManager();
        try {
            if($filem->createDir($dir)) {
                $this->resp_data = ['data' => true];
            } else {
                $this->addStatusHeader(500);
                $this->resp_data = ['error' => 'dir-not-created'];
            }
        } catch(\Exception $e) {
            $this->resp_data = ['error' => 'access-not-allowed'];
        }

        $this->respondJson();
    }

    public function renameDir($dir) {
        $this->resp_data = [];

        $to = $this->receive->get('to', FILTER_UNSAFE_RAW);

        if($to) {
            $filem = FeatureContainer::_fileManager();
            try {
                if($filem->renameDir($dir, $to)) {
                    $this->resp_data = ['data' => true];
                } else {
                    $this->addStatusHeader(500);
                    $this->resp_data = ['error' => 'dir-not-renamed'];
                }
            } catch(\Exception $e) {
                $this->resp_data = ['error' => 'access-not-allowed'];
            }
        } else {
            $this->resp_data = ['error' => 'rename-to-not-set'];
        }
        $this->respondJson();
    }

    public function deleteDir($dir) {
        $this->resp_data = [];

        $filem = FeatureContainer::_fileManager();
        try {
            if($filem->deleteDir($dir)) {
                $this->resp_data = ['data' => true];
            } else {
                $this->addStatusHeader(500);
                $this->resp_data = ['error' => 'dir-not-deleted'];
            }
        } catch(\Exception $e) {
            if('dir-not-empty' === $e->getMessage()) {
                $this->resp_data = ['error' => 'dir-not-empty'];
            } else if('dir-not-in-root' === $e->getMessage()) {
                $this->resp_data = ['error' => 'access-not-allowed'];
            }
        }
        $this->respondJson();
    }
}