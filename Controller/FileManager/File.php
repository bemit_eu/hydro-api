<?php

namespace HydroApi\Controller\FileManager;

use Hydro\Input\Receive;
use HydroApi\BaseController\BaseController;
use HydroFeature\Container as FeatureContainer;

class File extends BaseController {
    protected $receive;

    public function __construct() {
        parent::__construct();

        $this->receive = new Receive();
    }

    /**
     * @param $dir
     *
     * @throws \Exception
     */
    public function receiveFile($dir) {
        $this->resp_data = [];

        // api can send data, but currently nothing done with
        $data = $this->receive->get('data', FILTER_UNSAFE_RAW);

        if(isset($_FILES['file'])) {
            $file = $_FILES['file'];

            if($file['error']) {
                $this->addStatusHeader(500);
                $this->resp_data = ['error' => 'generic-receiving-error'];
            } else if(!$file['tmp_name']) {
                $this->addStatusHeader(500);
                $this->resp_data = ['error' => 'no-file-received'];
            } else {
                $filem = FeatureContainer::_fileManager();
                $done = $filem->handleFileUpload($file, $dir);
                if(true === $done) {
                    $this->resp_data = ['success' => $file['name']];
                } else if(-1 === $done) {
                    $this->resp_data = ['error' => 'file-already-exists'];
                } else {
                    $this->addStatusHeader(500);
                    $this->resp_data = ['error' => 'can-not-save'];
                }
            }
        } else {
            $this->resp_data = ['error' => true];
        }

        $this->respondJson();
    }

    public function renameFile($file) {
        $this->resp_data = [];

        $to = $this->receive->get('to', FILTER_UNSAFE_RAW);

        if($to) {
            $filem = FeatureContainer::_fileManager();
            try {
                if($filem->renameFile($file, $to)) {
                    $this->resp_data = ['data' => true];
                } else {
                    $this->addStatusHeader(500);
                    $this->resp_data = ['error' => 'file-not-renamed'];
                }
            } catch(\Exception $e) {
                $this->resp_data = ['error' => 'access-not-allowed'];
            }
        } else {
            $this->addStatusHeader(400);
            $this->resp_data = ['error' => 'rename-to-not-set'];
        }
        $this->respondJson();
    }

    public function deleteFile($file) {
        $this->resp_data = [];

        $filem = FeatureContainer::_fileManager();
        try {
            if($filem->deleteFile($file)) {
                $this->resp_data = ['data' => true];
            } else {
                $this->addStatusHeader(500);
                $this->resp_data = ['error' => 'file-not-deleted'];
            }
        } catch(\Exception $e) {
            $this->resp_data = ['error' => 'access-not-allowed'];
        }
        $this->respondJson();
    }
}