<?php

namespace HydroApi\Action;

// todo: add also generic CRUD actions to restrict to be able that an user can link from one hook's article to another but is not able to edit this article maybe even an `read-but-not-view-admin` together with an `read` action to distinct between "linking search allowed" and "view the article in the admin editor but not beeing able to save without the UPDATE action ability"

class Definition {
    private const PREFIX = 'hook.api:';
    public const ACTION_ACCESS_AUTH_SOURCE = self::PREFIX . 'access-auth-source';
    public const ACTION_CHECK_ALLOWED = self::PREFIX . 'check-allowed';

    /*
     * ADMIN VIEW CONSTANTS
     */
    private const PREFIX_VIEW = 'hook.api.view.access:';
    public const ACTION_ACCESS_ADMIN = self::PREFIX_VIEW . 'admin';

    public const ACTION_ACCESS_FEATURE_MESSAGE = self::PREFIX_VIEW . 'feature.message';

    public const ACTION_ACCESS_FEATURE_CONTENT = self::PREFIX_VIEW . 'feature.content';
    public const ACTION_ACCESS_FEATURE_CONTENT_SECTION = self::PREFIX_VIEW . 'feature.content.section';
    public const ACTION_ACCESS_FEATURE_CONTENT_ARTICLE = self::PREFIX_VIEW . 'feature.content.article';

    public const ACTION_ACCESS_FEATURE_SHOP = self::PREFIX_VIEW . 'feature.shop';
    public const ACTION_ACCESS_FEATURE_SHOP_SETTING = self::PREFIX_VIEW . 'feature.shop.setting';
    public const ACTION_ACCESS_FEATURE_SHOP_ORDER = self::PREFIX_VIEW . 'feature.shop.order';
    public const ACTION_ACCESS_FEATURE_SHOP_GROUP = self::PREFIX_VIEW . 'feature.shop.group';
    public const ACTION_ACCESS_FEATURE_SHOP_PRODUCT = self::PREFIX_VIEW . 'feature.shop.product';

    public const ACTION_ACCESS_FEATURE_INVENTORY = self::PREFIX_VIEW . 'feature.inventory';
    // can create/edit/delete warehouses, stocks, and stock_items
    public const ACTION_ACCESS_FEATURE_INVENTORY_ADMIN = self::PREFIX_VIEW . 'feature.inventory.admin';
    // can read warehouses, stocks and stock_items
    public const ACTION_ACCESS_FEATURE_INVENTORY_READ = self::PREFIX_VIEW . 'feature.inventory.read';
    // can create/edit/delete stock_items
    public const ACTION_ACCESS_FEATURE_INVENTORY_ITEMS_EDIT = self::PREFIX_VIEW . 'feature.inventory.items-edit';

    public const ACTION_ACCESS_FEATURE_MARKETING = self::PREFIX_VIEW . 'feature.marketing';
    public const ACTION_ACCESS_FEATURE_MARKETING_NEWSLETTER = self::PREFIX_VIEW . 'feature.marketing.newsletter';

    public const ACTION_ACCESS_FEATURE_MEDIA = self::PREFIX_VIEW . 'feature.media';

    public const ACTION_ACCESS_FEATURE_USER = self::PREFIX_VIEW . 'feature.user';

    public const ACTION_ACCESS_FEATURE_ANALYZE = self::PREFIX_VIEW . 'feature.analyze';

    public const ACTION_ACCESS_FEATURE_BLOCKS = self::PREFIX_VIEW . 'feature.blocks';

    public const ACTION_ACCESS_FEATURE_CONNECTIONS = self::PREFIX_VIEW . 'feature.connections';

}