<?php

namespace HydroApi\Action;

use HydroFeature\User\AccessManager\Action\ActionRestricted;
use HydroFeature\User\AccessManager\Action\BaseAction;

class AccessAuthSource extends BaseAction implements ActionRestricted {
    public function __construct($restriction = false) {
        parent::__construct($restriction);

        $this->setId(Definition::ACTION_ACCESS_AUTH_SOURCE);
    }
}